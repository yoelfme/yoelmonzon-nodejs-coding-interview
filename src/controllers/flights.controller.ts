import {
    JsonController,
    Get,
    Param,
    Put,
    Post,
    Body,
    Delete,
    OnUndefined,
} from 'routing-controllers';
import { FlightsService } from '../services/flights.service';

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    private _flightsService: FlightsService;

    constructor() {
        this._flightsService = new FlightsService();
    }

    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await this._flightsService.getFlights(),
        };
    }

    @Get('/:code')
    async getFlight(@Param('code') code: string) {
        return {
            status: 200,
            data: await this._flightsService.getFlight(code),
        };
    }

    @Put('/:code')
    async updateFlightStatus(@Param('code') code: string) {
        return {
            status: 200,
            data: await this._flightsService.updateFlightStatus(code),
        };
    }

    @Delete('/:code/passengers/:passport')
    @OnUndefined(204)
    async removePassenger(@Param('code') code: string, @Param('passport') passport: string) {
        return this._flightsService.removePassenger(code, passport)
    }

    // add flight
    @Post('')
    async addFlight(
        @Body()
        flight: {
            code: string;
            origin: string;
            destination: string;
            status: string;
        },
    ) {
        return {
            status: 200,
            data: await this._flightsService.addFlight(flight),
        };
    }
}
