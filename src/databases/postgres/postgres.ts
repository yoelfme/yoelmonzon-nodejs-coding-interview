import { Database } from '../database_abstract';

import { newDb, IMemoryDb } from 'pg-mem';

export class PostgreStrategy extends Database {
    _instance: IMemoryDb;

    constructor() {
        super();
        this.getInstance();
    }

    private async getInstance() {
        const db = newDb();

        db.public.many(`
            CREATE TABLE flights (
                code VARCHAR(5) PRIMARY KEY,
                origin VARCHAR(50),
                destination VARCHAR(50),
                status VARCHAR(50)
            );
        `);

        db.public.many(`
            CREATE TABLE passengers (
                passport VARCHAR(10) PRIMARY KEY,
                name VARCHAR(50)
            )
        `)

        db.public.many(`
            CREATE TABLE flights_passengers (
                id SERIAL PRIMARY KEY,
                flight_code VARCHAR(5),
                passenger_passport VARCHAR(10),
                CONSTRAINT fk_flight
                    FOREIGN KEY(flight_code) 
	                REFERENCES flights(code),
                CONSTRAINT fk_passenger
                    FOREIGN KEY(passenger_passport) 
	                REFERENCES passengers(passport),
                UNIQUE (flight_code, passenger_passport)
            );
        `)

        db.public.many(`
            INSERT INTO flights (code, origin, destination, status)
            VALUES ('LH123', 'Frankfurt', 'New York', 'on time'),
                     ('LH124', 'Frankfurt', 'New York', 'delayed'),
                        ('LH125', 'Frankfurt', 'New York', 'on time')
        `); 

        db.public.many(`
            INSERT INTO passengers (passport, name)
            VALUES ('12345', 'Yoel Monzon'), ('54321', 'Fransua Estrada')
        `)

        db.public.many(`
            INSERT INTO flights_passengers(flight_code, passenger_passport)
            VALUES ('LH123', '12345'), ('LH123', '54321')
        `)

        PostgreStrategy._instance = db;

        return db;
    }

    public async getFlights() {
        return PostgreStrategy._instance.public.many('SELECT * FROM flights');
    }

    public async getFlight(code: string) {
        return PostgreStrategy._instance.public.one(`
            SELECT *
            FROM flights f
            WHERE code = '${code}'
        `);
    }

    public async getPassengersFlight(code: string) {
        return PostgreStrategy._instance.public.many(`
            SELECT p.*
            FROM flights f
            INNER JOIN flights_passengers fp on f.code = fp.flight_code
            INNER JOIN passengers p on fp.passenger_passport = p.passport
            WHERE code = '${code}'
        `);
    }

    public async removePassenger(code: string, passport: string): Promise<void> {
        return PostgreStrategy._instance.public.none(`
            DELETE FROM flights_passengers 
            WHERE flight_code = '${code}' AND passenger_passport = '${passport}';
        `);
    }

    public async addFlight(flight: {
        code: string;
        origin: string;
        destination: string;
        status: string;
    }) {
        return PostgreStrategy._instance.public.many(
            `INSERT INTO flights (code, origin, destination, status) VALUES ('${flight.code}', '${flight.origin}', '${flight.destination}', '${flight.status}')`,
        );
    }
}
