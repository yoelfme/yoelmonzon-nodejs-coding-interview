interface Flight {
    code: string
    origin: string
    destination: string
    status: string
}
export class Database {
    public static _instance: any;

    public static getInstance() {
        // subclass must implement this method
    }

    public async getFlight(code: string) {
    }

    public async getPassengersFlight(code: string) {
    }

    public async removePassenger(code: string, passport: string) {}

    public async getFlights() {
        // subclass must implement this method
    }

    public async updateFlightStatus(code: string) {
        // subclass must implement this method
    }

    public async addFlight(flight: {
        code: string;
        origin: string;
        destination: string;
        status: string;
    }) {
        // subclass must implement this method
    }
}
