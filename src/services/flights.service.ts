import { Database } from '../databases/database_abstract';
import { DatabaseInstanceStrategy } from '../database';

export class FlightsService {
    private readonly _db: Database;

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance();
    }

    public async getFlights() {
        return this._db.getFlights();
    }

    public async getFlight(code: string) {
        const flight = await this._db.getFlight(code) as any;
        const passengers = await this._db.getPassengersFlight(code);

        return {
            ...flight,
            passengers
        }
    }

    public async removePassenger(code: string, passport: string) {
        return this._db.removePassenger(code, passport);
    }

    public async updateFlightStatus(code: string) {
        return this._db.updateFlightStatus(code);
    } 

    public async addFlight(flight: {
        code: string;
        origin: string;
        destination: string;
        status: string;
    }) {
        return this._db.addFlight(flight);
    }
}
